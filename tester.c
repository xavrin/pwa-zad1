#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <string.h>
#include "bcd.h"

int main (int argc, char* args[]) {
    char *buf1, *buf2, *buf3;
    int amount, max_len;
    int i;
    scanf("%d %d", &amount, &max_len);
    buf1 = malloc(max_len);
    buf2 = malloc(max_len);
    buf3 = malloc(max_len * 2);

    for (i = 0; i < amount; i++) {
        int c;
        char *res;
        bcd *iarg1, *iarg2, *ires;
        scanf("%d %s %s %s", &c, buf1, buf2, buf3);
        iarg1 = parse(buf1);
        iarg2 = parse(buf2);
        switch (c) {
            case 1:
                ires = suma(iarg1, iarg2); break;
            case 2:
                ires = roznica(iarg1, iarg2); break;
            case 3:
                ires = iloczyn(iarg1, iarg2); break;
            case 4:
                ires = iloraz(iarg1, iarg2); break;
            default:
                assert(!"bad number of operation");
        }
        res = unparse(ires);
        if (strcmp(res, buf3) != 0) {
            printf("%s %d %s --> '%s' != '%s'\n", buf1, c, buf2, res, buf3);
            return 0;
        }
        free(ires);
        free(res);
        free(iarg1);
        free(iarg2);
    }
    free(buf1);
    free(buf2);
    free(buf3);
    return 0;
}
