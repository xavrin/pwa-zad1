all: test tester

tester: tester.c bcd.o
	gcc -g -m32 -o $@ $^

test: test_main.c bcd.o
	gcc -g -m32 -o $@ $^

.SECONDARY:

%.o: %.asm
	nasm -g -f elf32 $<

%: %.o
	ld -m elf_i386 $< -o $@ -lc --dynamic-linker=/lib/ld-linux.so.2

clean:
	rm -f *.o
	rm -f test
