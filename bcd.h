#ifndef __BCD__H__
#define __BCD__H__

typedef struct bcd bcd;

bcd* parse(char *caption);

char* unparse(bcd *number);

bcd* suma(bcd *a, bcd *b);

bcd* roznica(bcd* a, bcd *b);

bcd* iloczyn(bcd *a, bcd *b);

bcd* iloraz(bcd *a, bcd *b);

#endif // __BCD__H__
