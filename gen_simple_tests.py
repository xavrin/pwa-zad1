#!/usr/bin/python

import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print ("Usage: " + sys.argv[0] +  " range")
        sys.exit(2)
    ran = int(sys.argv[1])
    ans = []
    print (4 * (2 * ran + 1) * (2 * ran + 1) - (2 * ran + 1)), ran
    for x in xrange(-ran, ran + 1):
        for y in xrange(-ran, ran + 1):
            for i in xrange(1, 5):
                if i == 1:
                    print i, x, y, x + y
                elif i == 2:
                    print i, x, y, x - y
                elif i == 3:
                    print i, x, y, x * y
                else:
                    if y:
                        res = abs(x) / abs(y)
                        if x * y < 0:
                            res = -res
                        print i, x, y, res
