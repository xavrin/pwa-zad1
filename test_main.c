#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <string.h>
#include "bcd.h"

void test_parsing() {
    int i;
    char *tests[] = {
        "0",
        "-1",
        "1",
        "9",
        "10",
        "12",
        "-12",
        "123",
        "-123",
        "123456789",
        "-123456789",
        "-123456890",
        "123456890",
    };
    int n = (sizeof tests) / (sizeof *tests);
    for (i = 0; i < n; i++) {
        assert(strcmp(tests[i], unparse(parse(tests[i]))) == 0);
    }
}

void test_adding_basic() {
    char* vala[] = {
        "1234",
        "1",
        "9999999",
        "777",
    };
    char* valb[] = {
        "75",
        "1",
        "2",
        "333",
    };
    char* res[] = {
        "1309",
        "2",
        "10000001",
        "1110",
    };
    int n = (sizeof res) / (sizeof *res);
    int i;
    for (i = 0; i < n; i++)
        assert(!strcmp(res[i], unparse(suma(parse(vala[i]), parse(valb[i])))));
}

void test_subtraction_basic() {
    char* vala[] = {
        "101",
        "11",
        "1453453245234523452345234523453245234523452345324523452345",
        "100000000000000000000000000000000000000000000000000000000000000000",
        "5349058",
    };
    char* valb[] = {
        "1",
        "9",
        "1453453245234523452345234523453245234523452345324523452345",
        "99999999999999999999999999999999999999999999999999999999999999999",
        "435144",
    };
    char* res[] = {
        "100",
        "2",
        "0",
        "1",
        "4913914",
    };
    int n = (sizeof res) / (sizeof *res);
    int i;
    for (i = 0; i < n; i++) {
        char *v = unparse(roznica(parse(vala[i]), parse(valb[i])));
        assert(!strcmp(res[i], v));
    }
}

void test_subtraction_hard() {
    char* vala[] = {
        "30",
        "-70",
        "-4",
        "-10",
        "10000000000",
    };
    char* valb[] = {
        "10000",
        "40",
        "-4",
        "-99990",
        "1",
    };
    char* res[] = {
        "-9970",
        "-110",
        "0",
        "99980",
        "9999999999",
    };
    int n = (sizeof res) / (sizeof *res);
    int i;
    for (i = 0; i < n; i++) {
        char *v = unparse(roznica(parse(vala[i]), parse(valb[i])));
        assert(!strcmp(res[i], v));
    }
}

void test_adding_hard() {
    char* vala[] = {
        "-9990990",
        "100000000000000000000000000000000000000000000000000000000",
        "10",
        "100",
        "1000",
    };
    char* valb[] = {
        "-10",
        "-1",
        "-1",
        "-1",
        "-1",
    };
    char* res[] = {
        "-9991000",
        "99999999999999999999999999999999999999999999999999999999",
        "9",
        "99",
        "999",
    };
    int n = (sizeof res) / (sizeof *res);
    int i;
    for (i = 0; i < n; i++) {
        char *v = unparse(suma(parse(vala[i]), parse(valb[i])));
        assert(!strcmp(res[i], v));
    }
}

void test_mul() {
    char* vala[] = {
        "418485",
        "509283445",
        "9999",
        "0",
        "2",
        "1000",
        "99",
        "-1",
        "1000000000000",
        "-190823405",
    };
    char* valb[] = {
        "836384",
        "-4395390485",
        "-9999",
        "1",
        "-2",
        "10000",
        "99",
        "-1",
        "100000000000000",
        "908603468099",
    };
    char* res[] = {
        "350014158240",
        "-2238499608321020825",
        "-99980001",
        "0",
        "-4",
        "10000000",
        "9801",
        "1",
        "100000000000000000000000000",
        "-173382807577460057095",
    };
    int n = (sizeof res) / (sizeof *res);
    int i;
    for (i = 0; i < n; i++) {
        char *v = unparse(iloczyn(parse(vala[i]), parse(valb[i])));
        assert(!strcmp(res[i], v));
    }
}

void test_div_free_safe() {
    char* vala[] = {
        "-902",
        "123948214",
        "-300",
        "4",
        "1",
        "1000000000000",
        "598345793845",
    };
    char* valb[] = {
        "3",
        "-593045",
        "7",
        "2",
        "-3",
        "1000000",
        "-3453095",
    };
    char* res[] = {
        "-300",
        "-209",
        "-42",
        "2",
        "0",
        "1000000",
        "-173278",
    };
    int n = (sizeof res) / (sizeof *res);
    int i;
    for (i = 0; i < n; i++) {
        bcd *ia = parse(vala[i]);
        bcd *ib = parse(valb[i]);
        bcd *il = iloraz(ia, ib);
        char *v = unparse(il);
        assert(!strcmp(res[i], v));
        free(ia);
        free(ib);
        free(il);
        free(v);
    }
}
int main (int argc, char* args[]) {
    test_parsing();
    test_adding_basic();
    test_adding_hard();
    test_subtraction_basic();
    test_subtraction_hard();
    test_mul();
    test_div_free_safe();
    return 0;
}
