#!/usr/bin/python

import sys
import random

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print ("Usage: " + sys.argv[0] +  " max_len_left max_len_right amount")
        sys.exit(2)
    max_len_left = int(sys.argv[1])
    max_len_right = int(sys.argv[2])
    amount = int(sys.argv[3])
    print amount, max(max_len_left, max_len_right)
    for _i in xrange(amount):
        j = random.randint(1, 4)
        a = random.getrandbits(max_len_left)
        if random.randint(0, 1) == 1:
            a = -a;
        b = random.getrandbits(max_len_right)
        if random.randint(0, 1) == 1:
            b = -b
        if j == 1:
            print j, a, b, a + b
        elif j == 2:
            print j, a, b, a - b
        elif j == 3:
            print j, a, b, a * b
        elif j == 4:
            while b == 0:
                b = random.getrandbits(max_len_right)
            res = abs(a) / abs(b)
            if (a < 0 and b > 0) or (a > 0 and b < 0):
                res = -res
            print j, a, b, res
