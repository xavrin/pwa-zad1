; Dawid Lazarczyk, 337614
section .text
global parse
global unparse
global suma
global roznica
global iloczyn
global iloraz

extern malloc
extern free

; external format - null terminated string with only digits, or minus sign at
; beginning
; internal format - first sign byte, then bcd bytes from LSB to MSB, then the
; byte for the end

%macro PROLOGUE 0
    PROLOGUE 0
%endmacro

%macro PROLOGUE 1
    enter %1, 0
    push ebx
    push edi
    push esi
    pushf
%endmacro

%macro EPILOGUE 0
    popf
    pop esi
    pop edi
    pop ebx
    leave
    ret
%endmacro

; %1, %2 - registers to swap, %3 - temporary register to use
%macro SWAP 3
    mov %3, %1
    mov %1, %2
    mov %2, %3
%endmacro

; find the argument in the string pointed by %edi, assumes that it exists in
; the string; affects eax, ecx, edi
%macro FIND 1
    cld ; setting the direction flag to increase addresses
    mov ecx, -1 ; disabling ecx from using to terminate
    mov al, %1 ; we'll look for al register value in the string
    repne scasb
%endmacro

%macro CHECK_MALLOC 1
    cmp eax, 0 ; checking if malloc was successful
    je %1
%endmacro

%macro CALL_MALLOC 1
    push %1
    call malloc
    pop %1
%endmacro

; multiplication of one byte bcd value kept in eax register and value in the
; first 32-bit argument (%1), we also need a temporary 32-bit register (%2)
; the third argument contains first 8-bit part of %1
; the forth argument contains second 8-bit part of %1
; the fifth argument contains first 8-bit part of the temporary register (%2)
%macro MUL 5
    mov %2, eax
    shr %2, 4
    shl %2, 1
    lea %2, [%2 + 2 * %2] ; %2 contains 6 * MSB of eax
    sub eax, %2 ; now eax contains correct binary value

    mov %2, %1
    shr %2, 4
    shl %2, 1
    lea %2, [%2 + 2 * %2] ; %2 contains 6 * MSB of %1
    sub %1, %2 ; now %1 contains correct binary value

    mul %3
    ; now we have to transform eax back to bcd form
    mov %5, 100
    div %5
    ; ah contains remainder, al - quotient
    mov %1, eax ; saving the results for later
    mov eax, 0
    mov al, %4
    mov %5, 10
    div %5
    shl al, 4
    add al, ah
    ; now al contains the first byte of the result
    mov %4, al
    mov al, %3
    mov ah, 0
    div %5
    shl al, 4
    add ah, al
    mov al, %4
%endmacro

parse:
    PROLOGUE
    ; checking the sign of input
    mov ebx, 0
    mov edi, [ebp + 8]
    cmp byte [edi], ASCII_MINUS
    jne no_sign
    mov ebx, 1
    inc edi
no_sign:
    mov edx, edi ; edx will hold the beginning of the number, ignoring the sign
    FIND 0
    ; size = ceil((input_bytes + 2 for terminating symbol + 2 for sign) / 2)
    ; it's divided by 2 because it's packed bcd
    sub edi, 1
    mov ecx, edi
    sub ecx, edx ; calculating the distance between beginning and end of input
    add ecx, 5
    shr ecx, 1 ; dividing by 2

    push edx
    CALL_MALLOC ecx
    pop edx
    CHECK_MALLOC parse.epilogue

    ; calculating the sign of input
    cmp ebx, 1
    je get_sign.minus
    mov bl, PLUS
    jmp get_sign.end
get_sign.minus:
    mov bl, MINUS
get_sign.end:
    ; bl will hold the value of currently created byte
    mov esi, edi
    sub esi, 1 ; esi points to the end of input (i.e. last non-NULL byte)
    mov edi, eax ; edi points to the beginning of output

    mov [edi], bl ; writing the sign byte
    inc edi
    mov bl, POISON
    std ; setting the direction flag to decreasing order
assign.iter:
    lodsb ; read from esi to al, decrement esi
    sub al, ASCII_ZERO
    cmp bl, POISON ; checking which part of byte it is this time
    je assign.lower_half
    ; now we have to write the byte
    shl al, 4
    add bl, al
    mov [edi], bl
    inc edi
    mov bl, POISON
    jmp assing.if_end
assign.lower_half:
    mov bl, al
assing.if_end:
    cmp esi, edx
    jae assign.iter

    ; perhaps writing the MSB
    cmp bl, POISON
    je nothing
    mov [edi], bl
    inc edi
nothing:
    ; now we have to write the end sign
    mov byte [edi], THE_END
    sub edi, ecx
    add edi, 1
    mov eax, edi
parse.epilogue:
    EPILOGUE

unparse:
    PROLOGUE
    mov edi, [ebp + 8]

    mov edx, 0 ; clearing edx
    ; checking the sign of input
    cmp byte [edi], MINUS
    sete dl

    FIND THE_END
    sub edi, 1
    mov ecx, edi
    sub ecx, [ebp + 8]
    sub ecx, 1 ; subtracting because of 'sign' and 'end' bytes,
               ; however adding 1 to count bytes correctly
    shl ecx, 1 ; multipling because of the packing
    add ecx, 1 ; for the NULL byte
    add ecx, edx ; adding 1 in case of the minus sign

    mov dh, 0
    cmp byte [edi - 1], 10
    ja unparse.no_sub
    sub ecx, 1 ; input had odd number of bytes
    mov dh, 1
unparse.no_sub:

    push edx
    CALL_MALLOC ecx
    pop edx
    CHECK_MALLOC unparse.epilogue

    mov ebx, eax ; saving malloced address

    mov esi, edi
    sub esi, 1 ; now esi shows the last meaningful byte in the input
    mov edi, eax ; edi shows beginning of the output

    cmp dl, 0
    je unparse.no_sign_needed
    mov byte [edi], ASCII_MINUS
    add edi, 1
unparse.no_sign_needed:

    cmp dh, 1
    jne unparse.no_half_byte
    mov al, [esi]
    sub esi, 1
    add al, ASCII_ZERO
    stosb
unparse.no_half_byte:

    mov ecx, [ebp + 8]
    cmp esi, ecx
    je unparse.iter.end
unparse.iter:
    mov dl, [esi]
    mov al, dl
    shr al, 4
    add al, ASCII_ZERO
    stosb
    mov al, dl
    and al, 15
    add al, ASCII_ZERO
    stosb
    sub esi, 1
    cmp esi, ecx
    ja unparse.iter
unparse.iter.end:

    mov al, 0 ; writing the terminating NULL
    stosb
    mov eax, ebx
unparse.epilogue:
    EPILOGUE

; adding absolute value of %2 to %1,
; assuming that %1 has bigger a.v. than %2
adding:
    PROLOGUE
    mov edi, [ebp + 8]
    FIND THE_END
    mov ecx, edi
    sub ecx, [ebp + 8]
    add ecx, 1

    CALL_MALLOC ecx
    CHECK_MALLOC adding.epilogue

    mov edx, eax ; saving the beginning of the allocated memory
    mov edi, [ebp + 8] ; the bigger number from the beginning
    mov esi, [ebp + 12] ; the smaller number from the beginning
    mov ebx, edx
    ; setting the sign
    mov ecx, [ebp + 16]
    mov byte [ebx], cl
    inc ebx
    inc edi
    inc esi
    pushf
    ; iterating until the smaller number ends
adding.iter1:
    popf
    mov al, [edi]
    adc al, [esi]
    daa
    mov [ebx], al
    inc edi
    inc esi
    inc ebx
    pushf
    cmp byte [esi], THE_END
    jne adding.iter1
    ; iterating until the bigger number ends
    cmp byte [edi], THE_END
    je adding.iter.end
adding.iter2:
    popf
    mov al, [edi]
    adc al, 0
    daa
    mov [ebx], al
    inc edi
    inc ebx
    pushf
    cmp byte [edi], THE_END
    jne adding.iter2
adding.iter.end:
    popf
    jnc adding.no_carry
    mov byte [ebx], 1
    inc ebx
adding.no_carry:

    mov byte [ebx], THE_END
    mov eax, edx
adding.epilogue:
    EPILOGUE

; subtracting absolute value of %2 from %1,
; assuming that %1 has bigger a.v. than %2
subtracting:
    PROLOGUE
    mov edi, [ebp + 8]
    FIND THE_END
    mov ecx, edi
    sub ecx, [ebp + 8]

    CALL_MALLOC ecx
    CHECK_MALLOC subtracting.epilogue

    mov edx, eax ; saving the beginning of the allocated memory
    mov edi, [ebp + 8] ; the bigger number from the beginning
    mov esi, [ebp + 12] ; the smaller number from the beginning
    mov ebx, edx
    ; setting the sign
    mov ecx, [ebp + 16]
    mov byte [ebx], cl
    inc ebx
    inc edi
    inc esi
    ; iterating until the smaller number ends
    pushf
subtracting.iter1:
    popf
    mov al, [edi]
    sbb al, [esi]
    das
    mov [ebx], al
    inc edi
    inc esi
    inc ebx
    pushf
    cmp byte [esi], THE_END
    jne subtracting.iter1
    ; iterating until the bigger number ends
    cmp byte [edi], THE_END
    je subtracting.iter.end
subtracting.iter2:
    popf
    mov al, [edi]
    sbb al, 0
    das
    mov [ebx], al
    inc edi
    inc ebx
    pushf
    cmp byte [edi], THE_END
    jne subtracting.iter2
subtracting.iter.end:
    popf
    ; removal of the leading zeroes
    mov eax, edx
    inc eax
subtracting.iter.leading_zeroes:
    dec ebx
    cmp byte [ebx], 0
    jne subtracting.iter.leading_zeroes.end
    cmp ebx, eax
    ja subtracting.iter.leading_zeroes
subtracting.iter.leading_zeroes.end:
    inc ebx
    mov byte [ebx], THE_END
    dec eax

subtracting.epilogue:
    EPILOGUE

; subtracts absolute value of %2 from %1 and places the result in %1,
; it ignores the signs, it doesn't check them
; assuming that %1 is bigger than %2
subtracting_in_place:
    PROLOGUE
    mov edi, [ebp + 8] ; the bigger number from the beginning
    mov esi, [ebp + 12] ; the smaller number from the beginning
    ; forgetting the sign bytes
    inc edi
    inc esi
    pushf
    ; iterating until the smaller number ends
subtracting_in_place.iter1:
    popf
    mov al, [edi]
    sbb al, [esi]
    das
    mov [edi], al
    inc edi
    inc esi
    pushf
    cmp byte [esi], THE_END
    jne subtracting_in_place.iter1
    ; iterating until the bigger number ends
    cmp byte [edi], THE_END
    je subtracting_in_place.iter.end
subtracting_in_place.iter2:
    popf
    mov al, [edi]
    sbb al, 0
    das
    mov [edi], al
    inc edi
    pushf
    cmp byte [edi], THE_END
    jne subtracting_in_place.iter2
subtracting_in_place.iter.end:
    popf
    mov esi, edi ; esi points to THE_END
    dec edi ; edi points to the last byte before end
    ; now we want to make a shift in order to delete leading zeroes and not to
    ; loose memory
    mov ebx, [ebp + 8]
    inc ebx
subtracting_in_place.iter.zeroes:
    dec esi
    cmp esi, ebx
    jb subtracting_in_place.iter.zeroes.end
    cmp byte [esi], 0
    je subtracting_in_place.iter.zeroes
subtracting_in_place.iter.zeroes.end:
    ; esi points to the first meaningful byte (or, if the result is 0, to the
    ; sign byte)
    cmp esi, ebx
    jb subtracting_in_place.single_zero
    cmp esi, edi
    je subtracting_in_place.no_zeroes
    ; now we want to do a shift removing the leading zeroes
subtracting_in_place.iter.zeroes_removal:
    mov al, [esi]
    mov [edi], al
    dec esi
    dec edi
    cmp esi, ebx
    jae subtracting_in_place.iter.zeroes_removal
    mov eax, edi
    jmp subtracting_in_place.epilogue
subtracting_in_place.single_zero:
    mov eax, edi
    dec eax
    jmp subtracting_in_place.epilogue
subtracting_in_place.no_zeroes:
    mov eax, [ebp + 8]
subtracting_in_place.epilogue:
    EPILOGUE


; compare absolute values of two input numbers
; it returns a boolean saying %1 is less or equal to %2
; it's guaranteed to ignore the sign byte
compare:
    PROLOGUE
    mov edi, [ebp + 8]
    inc edi
    FIND THE_END
    mov edx, edi
    sub edx, [ebp + 8]

    mov edi, [ebp + 12]
    inc edi
    FIND THE_END
    mov ecx, edi
    sub ecx, [ebp + 12]

    mov eax, 0
    mov ebx, 1 ; register for holding 1, cmovCC don't accept constants
    cmp edx, ecx
    je compare.equals
    cmovb eax, ebx
    jmp compare.epilogue
compare.equals:
    mov eax, 1 ; default value in case the numbers are the same
    mov ecx, 0 ; register for holding 0, cmovCC don't accept constants
    mov edi, [ebp + 8]
    inc edi
    mov esi, [ebp + 12]
    inc esi
compare.iter:
    mov dl, [esi]
    cmp byte [edi], dl
    cmovb eax, ebx
    cmova eax, ecx
    inc esi
    inc edi
    cmp byte [esi], THE_END
    jne compare.iter
compare.epilogue:
    EPILOGUE

suma:
    PROLOGUE
    mov edi, [ebp + 8]
    mov esi, [ebp + 12]

    push esi
    push edi
    call compare
    pop edi
    pop esi

    mov ecx, 0 ; clearing the register
    cmp eax, 1
    jne suma.first_bigger
    SWAP esi, edi, ebx
suma.first_bigger:
    mov cl, [edi]

    push ecx
    push esi
    push edi
    cmp cl, [esi]
    jne suma.subtract
    call adding
    jmp suma.after_call
suma.subtract:
    call subtracting
suma.after_call:
    pop edi
    pop esi
    pop ecx
    ; now we want to make sure that sign is '+' when the number equals 0
    cmp byte [eax + 2], THE_END
    jne suma.epilogue
    cmp byte [eax + 1], 0
    jne suma.epilogue
    mov byte [eax], PLUS
suma.epilogue:
    EPILOGUE

roznica:
    PROLOGUE
    mov edi, [ebp + 8]
    mov esi, [ebp + 12]
    cmp byte [esi], PLUS
    je roznica.plus
    mov bl, MINUS
    mov byte [esi], PLUS
    jmp roznica.sign_changed
roznica.plus:
    mov bl, PLUS
    mov byte [esi], MINUS
roznica.sign_changed:
    push esi
    push edi
    call suma
    pop edi
    pop esi
    ; getting the original sign back
    mov byte [esi], bl
    EPILOGUE

iloczyn:
    PROLOGUE 3
    ; [ebp - 4] will serve for remembering the beginning of output
    ; [ebp - 8] will store carry
    ; [ebp - 12] will hold an offset for writing to the result

    mov edi, [ebp + 8]
    FIND THE_END
    mov edx, edi
    sub edx, [ebp + 8]

    mov edi, [ebp + 12]
    FIND THE_END
    mov ecx, edi
    sub ecx, [ebp + 12]

    add ecx, edx
    CALL_MALLOC ecx
    CHECK_MALLOC iloczyn.epilogue
    mov [ebp - 4], eax
    ; now we want to zero the memory ecx times
    mov edx, eax
    mov edi, edx
    mov eax, 0
    cld ; setting the right direction
    rep stosb
    dec edi
    mov byte [edi], THE_END
    ; now we want to start looping
    mov esi, [ebp + 8]
    mov edi, [ebp + 12]
    ; let's write the sign of the result
    mov al, [edi]
    cmp byte al, [esi]
    je iloczyn.plus
    mov cl, MINUS
    jmp iloczyn.write_sign
iloczyn.plus:
    mov cl, PLUS
iloczyn.write_sign:
    mov [edx], cl
    inc edx
    inc edi
    inc esi
    ; sign is written, let's start the main part
    mov byte [ebp - 8], 0 ; initially carry is equal to 0
    mov ebx, 0 ; cleaning ebx
    mov [ebp - 12], edx
iloczyn.iter:
    mov eax, 0
    mov ebx, 0
    mov al, [edi]
    mov bl, [esi]
    MUL ebx, ecx, bl, bh, cl
    add al, [edx]
    daa
    setc cl
    add al, [ebp - 8]
    daa
    setc ch
    add cl, ch
    ; adding carry's to the higher byte
    mov bl, al
    mov al, ah
    add al, cl
    daa
    mov byte [edx], bl
    mov byte [ebp - 8], al
    inc esi
    inc edx
    cmp byte [esi], THE_END
    jne iloczyn.iter
    ; we have to dispose of the carry
    mov bl, [ebp - 8]
    mov byte [ebp - 8], 0
    cmp bl, 0
    je iloczyn.carry.end
iloczyn.carry.iter:
    mov al, [edx]
    add al, bl
    daa
    mov [edx], al
    setc bl
    inc edx
    cmp bl, 0
    jne iloczyn.carry.iter
iloczyn.carry.end:
    inc edi
    cmp byte [edi], THE_END
    je iloczyn.after
    mov esi, [ebp + 8]
    inc esi ; forgetting the sign byte
    inc dword [ebp - 12]
    mov edx, [ebp - 12]
    jmp iloczyn.iter
iloczyn.after:
    mov edi, [ebp - 4]
    FIND THE_END
    dec edi
iloczyn.zeroes:
    dec edi
    cmp byte [edi], 0
    je iloczyn.zeroes
    cmp byte [edi], PLUS
    jne iloczyn.not_plus
    inc edi
    jmp iloczyn.finishing
iloczyn.not_plus:
    cmp byte [edi], MINUS
    jne iloczyn.finishing
    ; it means that the answer is 0 - we have to fix the sign
    mov byte [edi], PLUS
    inc edi
iloczyn.finishing:
    ; now edi points to MSB of the output
    inc edi
    mov byte [edi], THE_END
    mov eax, [ebp - 4]
iloczyn.epilogue:
    EPILOGUE

iloraz:
    PROLOGUE 2
    ; [ebp - 4] contains pointer to the buffer
    ; [ebp - 8] contains pointer to the result
    mov edi, [ebp + 8]
    FIND THE_END
    mov ecx, edi
    sub ecx, [ebp + 8]
    add ecx, 4 ; just for safety
    ; allocating the buffer
    CALL_MALLOC ecx
    CHECK_MALLOC iloraz.epilogue
    mov [ebp - 4], eax
    ; allocating place for the result
    CALL_MALLOC ecx
    CHECK_MALLOC iloraz.free
    mov [ebp - 8], eax
    mov edi, eax
    add edi, ecx
    dec edi
    mov byte [edi], THE_END
    dec edi
    mov esi, [ebp - 4]
    add esi, ecx
    dec esi
    mov byte [esi], THE_END
    dec esi
    mov edx, edi
    mov edi, [ebp + 8]
    FIND THE_END
    SWAP edi, edx, ecx
    sub edx, 2
    mov bl, 0 ; bl counts how many times buffer contains the second argument
    ; esi points to the MSB of the buffer, edi points to the MSB of the result
    ; edx points to the MSB of the first argument
iloraz.iter:
    mov al, [edx]
    mov [esi], al
    dec edx
    dec esi
iloraz.compare:
    push edx
    push esi
    push dword [ebp + 12]
    call compare
    pop ecx ; whatever
    pop esi
    pop edx

    cmp eax, 1
    jne iloraz.loopend
iloraz.subtract:

    push edx
    push dword [ebp + 12]
    push esi
    call subtracting_in_place
    pop esi
    pop ecx ; whatever
    pop edx

    mov esi, eax ; updating the buffer
    ; when we got 0, we should increment esi because 0 * 100 = 0
    cmp byte [esi + 2], THE_END
    jne iloraz.no_action
    cmp byte [esi + 1], 0
    jne iloraz.no_action
    inc esi
iloraz.no_action:
    ; incrementing the counter
    mov al, bl
    add al, 1
    daa
    mov bl, al
    jmp iloraz.compare
iloraz.loopend:
    mov byte [edi], bl
    mov bl, 0
    dec edi
    cmp edx, [ebp + 8]
    ja iloraz.iter
iloraz.end:
    ; writing the sign
    mov esi, [ebp + 12]
    mov al, [esi]
    cmp al, [edx]
    je iloraz.plus
    mov byte [edi], MINUS
    jmp iloraz.sign_end
iloraz.plus:
    mov byte [edi], PLUS
iloraz.sign_end:
    mov esi, edi
    FIND THE_END
    dec edi
iloraz.cut_zeroes:
    dec edi
    cmp byte [edi], 0
    je iloraz.cut_zeroes
    cmp edi, esi
    jne iloraz.no_single_zero
    mov byte [edi], PLUS
    inc edi
iloraz.no_single_zero:
    inc edi ; now edi points to the byte after MSB
    mov byte [edi], THE_END
    ; making a shift to the beginning, in order to pass the caller address
    ; returned by malloc
    mov edi, [ebp - 8]
    ; because we allocated to big buffer we don't have to check if esi != edi
iloraz.iter.shift:
    mov al, [esi]
    mov [edi], al
    inc esi
    inc edi
    cmp byte [esi], THE_END
    jne iloraz.iter.shift
    mov byte [edi], THE_END
    mov eax, [ebp - 8]
iloraz.free:
    push eax
    push dword [ebp - 4]
    call free
    pop ecx ; whatever
    pop eax
iloraz.epilogue:
    EPILOGUE

section .data

THE_END equ 255 ; the end of the number
POISON equ 14 ; value not used in representation, useful in the code
ASCII_MINUS equ 45
ASCII_ZERO equ 48
PLUS equ 204
MINUS equ 221
